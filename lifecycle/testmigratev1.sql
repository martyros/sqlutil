/* Migrate from version 1 to version 2 */
create table departments (
    department_id    integer primary key,
    department_abbrv text,
    department_full  text
);

alter table students add DOB date;

alter table classes add department_id references departments;

alter table classes add class_number;