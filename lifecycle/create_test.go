package lifecycle

import (
	_ "embed"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

//go:embed testschemav2.sql
var testschemav2 string

//go:embed testschemav1.sql
var testschemav1 string

//go:embed testmigratev1.sql
var testmigratev1 string

// TODO: Test the function half of recipes, both for creation and migration

func TestCreate(t *testing.T) {
	dbFile, err := os.CreateTemp("", "lifecycletest*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	db, err := sqlx.Open("sqlite3", "file:"+dbFile.Name()+"?_fk=true&mode=rwc")
	if err != nil {
		t.Errorf("Opening database: %v", err)
		return
	}

	lccfg := Schema(2, Recipe{Sql: testschemav2,
		Function: func(cfg *Config, eq sqlx.Ext) error {
			return cfg.SetParam(eq, "Foo", "Bar")
		}})

	err = lccfg.Open(db)
	if err != nil {
		t.Errorf("Initializing database: %v", err)
		return
	}

	// Calling Open twice in a row should work just fine
	err = lccfg.Open(db)
	if err != nil {
		t.Errorf("Re-initializing database: %v", err)
		return
	}

	if val, err := lccfg.GetParam(db, "Foo"); err != nil {
		t.Errorf("ERROR Getting parameter: %v", err)
	} else if val != "Bar" {
		t.Errorf("ERROR: Unexpected value for Foo: got %s wanted Bar!", val)
	}

	// Try opening the file using the old schema; should return an error
	t.Log("Opening schema v2 file with schema v1 code")
	oldcfg := Schema(1, Recipe{Sql: testschemav1})
	err = oldcfg.Open(db)
	if err == nil {
		t.Errorf("Attempt to open v2 file with v1 succeeded!")
	} else {
		t.Logf("Returned error: %v", err)
	}
}

func TestCreateTx(t *testing.T) {
	dbFile, err := os.CreateTemp("", "lifecycletest*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	db, err := sqlx.Open("sqlite3", "file:"+dbFile.Name()+"?_fk=true&mode=rwc")
	if err != nil {
		t.Errorf("Opening database: %v", err)
		return
	}

	lccfg := Schema(2, Recipe{Sql: testschemav2})

	if tx, err := db.Beginx(); err != nil {
		t.Errorf("ERROR Starting transaction: %v", err)
		return
	} else if err := lccfg.OpenTx(tx); err != nil {
		tx.Rollback()
		t.Errorf("ERROR Initializing database: %v", err)
		return
	} else if err := tx.Commit(); err != nil {
		t.Errorf("ERROR Committing transaction: %v", err)
		return
	}

	// Calling Open twice in a row should work just fine
	if tx, err := db.Beginx(); err != nil {
		t.Errorf("ERROR Starting transaction: %v", err)
		return
	} else if err := lccfg.OpenTx(tx); err != nil {
		tx.Rollback()
		t.Errorf("ERROR Re-initializing database: %v", err)
		return
	} else if err := tx.Commit(); err != nil {
		t.Errorf("ERROR Committing transaction: %v", err)
		return
	}

	// Try opening the file using the old schema; should return an error
	t.Log("Opening schema v2 file with schema v1 code")
	oldcfg := Schema(1, Recipe{Sql: testschemav1})
	if tx, err := db.Beginx(); err != nil {
		t.Errorf("ERROR Starting transaction: %v", err)
		return
	} else if err := oldcfg.OpenTx(tx); err == nil {
		tx.Rollback()
		t.Errorf("ERROR: Attempt to open v2 file with v1 succeeded!")
		return
	} else {
		tx.Rollback()
		t.Logf("Returned error: %v", err)
	}
}

func TestMigrate(t *testing.T) {
	dbFile, err := os.CreateTemp("", "lifecycletest*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	db, err := sqlx.Open("sqlite3", "file:"+dbFile.Name()+"?_fk=true&mode=rwc")
	if err != nil {
		t.Errorf("Opening database: %v", err)
		return
	}

	err = Schema(1, Recipe{Sql: testschemav1}).
		Open(db)
	if err != nil {
		t.Errorf("Initializing database: %v", err)
		return
	}

	t.Log("Attempting to open schemav2 with no automigrate")
	err = Schema(2, Recipe{Sql: testschemav2}).
		Open(db)
	if err == nil {
		t.Errorf("Expected failure!")
		return
	} else {
		t.Logf("Returned error %v", err)
	}

	t.Log("Attempting to open schemav2 with automigrate but no migration recipes")
	err = Schema(2, Recipe{Sql: testschemav2}).
		SetAutoMigrate(true).
		Open(db)
	if err == nil {
		t.Errorf("Expected failure!")
		return
	} else {
		t.Logf("Returned error %v", err)
	}

	t.Log("Attempting to open schemav2 with automigrate and a migration recipes")
	err = Schema(2, Recipe{Sql: testschemav2}).
		SetMigrationRecipe(1, Recipe{Sql: testmigratev1}).
		SetAutoMigrate(true).
		Open(db)
	if err != nil {
		t.Errorf("Migration failed: %v", err)
		return
	}

	t.Log("Attempting to open schemav2 after automigration")
	err = Schema(2, Recipe{Sql: testschemav2}).
		SetMigrationRecipe(1, Recipe{Sql: testmigratev1}).
		SetAutoMigrate(true).
		Open(db)
	if err != nil {
		t.Errorf("Migration failed: %v", err)
		return
	}

}

func TestSchemaName(t *testing.T) {
	// "Create a DB with no schemaname and try to open it w/ a config w/ no schemaname" is already tested
	tests := []struct {
		testName, initName, reopenName string
		shouldSucceed                  bool
	}{
		// Create a DB with no schemaname and try to open it w/ a config /w a schema name
		{"NoSchemaToSchema", "", "gitlab.com/martyros/sqlutil/lifecycle#a", false},
		// Create a DB with a schemaname and try to open it w/ a config w/ the same schema name
		{"SchemaToSameSchema", "gitlab.com/martyros/sqlutil/lifecycle#a", "gitlab.com/martyros/sqlutil/lifecycle#a", true},
		// Create a DB with a schemaname and try to open it w/ a config w/ a different schema name
		{"SchemaToDifferentSchema", "gitlab.com/martyros/sqlutil/lifecycle#a", "gitlab.com/martyros/sqlutil/lifecycle#b", false},
		// Create a DB with a schemaname and try to open it w/ a config w/ no schema name
		{"SchemaToNoSchema", "gitlab.com/martyros/sqlutil/lifecycle#a", "", false},
	}

	for i := range tests {
		test := tests[i]
		t.Run(test.testName, func(t *testing.T) {
			t.Parallel()

			dbFile, err := os.CreateTemp("", "lifecycletest*.sqlite")
			if err != nil {
				t.Errorf("Opening temp file for db operations: %v", err)
				return
			}

			t.Logf("Creating DB in file %s", dbFile.Name())

			db, err := sqlx.Open("sqlite3", "file:"+dbFile.Name()+"?_fk=true&mode=rwc")
			if err != nil {
				t.Errorf("Opening database: %v", err)
				return
			}

			t.Logf("Creating initial DB with schemaname '%s'", test.initName)
			initcfg := Schema(1, Recipe{Sql: testschemav1})

			if test.initName != "" {
				initcfg.SetSchemaName(test.initName)
			}

			if err := initcfg.Open(db); err != nil {
				t.Errorf("ERROR Initializing database: %v", err)
				return
			}

			t.Logf("Reopening DB with schemaname '%s'", test.reopenName)
			reopencfg := Schema(1, Recipe{Sql: testschemav1})

			if test.reopenName != "" {
				reopencfg.SetSchemaName(test.reopenName)
			}

			err = reopencfg.Open(db)

			if test.shouldSucceed && err != nil {
				t.Errorf("ERROR Unexpected failure: %v", err)
				return
			} else if !test.shouldSucceed && err == nil {
				t.Errorf("ERROR Unexpected success!")
				return
			}

			t.Logf("Reopen returned %v as expected", err)
		})
	}
}
