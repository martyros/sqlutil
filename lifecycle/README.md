# Overview

Package `lifecycle` is designed to simplify the setup of simple SQL-focused
sqlite3 database projects.

A common thing you want to do when handed a sqlite file is 1) Determine if it's
already initialized, or needs the tables created 2) Determine if it's the
correct schema version 3) Migrate to the newest schema version (if required).
The aim of this library is to do the grunt work for that.

# Quick-start

Given version 2 of a schema in `schemav2.sql`, and migration code to migrate
from version 1 to version 2 in `migratev1.sql`, the following code will:

 - Check to see if the database is initialized; if not, it will initialize a
   "params" table, write the current database version and schema name to it, and
   then execute schemav2.sql before returning
 - If it's initialized:
   - Check the schema name.
     - If it matches (where "both unspecified" is a match), fine
	 - If it's different (either different name, or present in one place but not the other), return an error
   - Check the version.
     - If it's the current version (2), it will return success.
     - If it's newer (>2), it will return an error
     - If it's older (1), it will migrate the database.

```go
//go:embed schemav2.sql
var schemav2 string

//go:embed migratev1.sql
var migratev1 string

func Open(filename string) (*sqlx.DB, error) {
	db, err := sqlx.Open("sqlite3", "file:"+filename+"?_fk=true&mode=rwc")
	if err != nil {
		return nil, fmt.Errorf("Opening database: %w", err)
	}

  err = lifecycle.Schema(2, lifecycle.Recipe{Sql: schemav2}).
    SetSchemaName("gitlab.com/user/module/package#db").
		SetMigrationRecipe(1, lifecycle.Recipe{Sql: migratev1}).
		SetAutoMigrate(true).
		Open(db)
	if err != nil {
    db.Close()
		return nil, fmt.Errorf("Init / migrate failed: %w", err)
	}

  return db, nil
}
```

# To-do

 - [ ] Add an example directory, perhaps with v1 / v2 code to see how it works
 - [ ] Have `Open` take a driver / address (i.e., filename) argument
 - [ ] Have a method which returns an sqlx database
 - [ ] Do proper testing of "parallel upgrades", which may require a transaction loop
 - [ ] See if we can add code to sanity-check the results of migrations; i.e.,
   make an empty database w/ the current version, and compare the tables
 - [ ] Add caller-supplied sanity checks for the contents of the databases
 - [ ] Add more tests for transaction-based updates
