package lifecycle

import (
	"errors"
	"os"
	"testing"
)

func TestLifecycleError(t *testing.T) {
	// Test 1: ErrorNeedsUpgrade.Is()
	err1 := ErrorNeedsUpgrade.errorf("Database version %d, current version %d. Please migrate.", 1, 2)
	err2 := ErrorNeedsUpgrade.errorf("Database version %d, current version %d. Please migrate.", 2, 3)
	if !errors.Is(err1, ErrorNeedsUpgrade) || !errors.Is(err2, ErrorNeedsUpgrade) {
		t.Errorf("ErrorNeedsUpgrade.Is() failed")
	}

	// Test 2: Error formatting and Error() method
	expectedErrMsg := "Database version 1, current version 2. Please migrate."
	if err1.Error() != expectedErrMsg {
		t.Errorf("Error formatting failed, expected %q, got %q", expectedErrMsg, err1.Error())
	}

	// Test 3: Wrapping error with %w and using errors.Is
	wrappedErr := ErrorNeedsUpgrade.errorf("%w", os.ErrClosed)
	if !errors.Is(wrappedErr, os.ErrClosed) {
		t.Errorf("errors.Is with wrapped error failed")
	}
}
