// Package lifecycle is designed to simplify the setup of simple SQL-focused
// sqlite3 database projects.
//
// A common thing you want to do when handed a sqlite file is 1) Determine if
// it's already initialized, or needs the tables created 2) Determine if it's
// the correct schema version 3) Migrate to the newest schema version (if
// required).  The aim of this library is to do the grunt work for that.
package lifecycle

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
)

const (
	PARAMS_KEY_DBVERSION    = "dbversion"
	PARAMS_KEY_DBSCHEMANAME = "dbschemaname"
)

// Recipe contains a recipe for doing something with the database; for instance,
// initializing an empty database, or migrating from version N of a database to
// version N+1.  Sql is sql which is executed (as part of the transaction).
// Function is a function called with the transaction handle.
//
// If both are present, Sql is executed first, then Function called.
//
// If either one results in an error, the migration will stop and the entire
// transaction will be rolled back.
type Recipe struct {
	Sql      string
	Function func(*Config, sqlx.Ext) error
}

// Config is the configuration for database initialization and migration.  It
// should be created with the Schema() function.
//
// Note that some error checking is done as the configuration is assembled; but
// those errors cannot be returned immediately; they will only be returned when
// Config.Open() or Config.Check() are called.
//
// If an error happens in any of the directives, the rest of the directives are
// skipped.
type Config struct {
	err            error
	schemaName     string
	initRecipe     Recipe
	swDbVersion    int
	paramTableName string
	autoMigrate    bool
	migrateRecipes []*Recipe
}

var configDefault = Config{
	paramTableName: "params",
}

// Check to see if the assembled config resulted in any errors.
func (cfg *Config) Check() error {
	return cfg.err
}

// Schema creates a basic configuration: The current database version, and the current schema to
// create the database.
func Schema(swDbVersion int, initRecipe Recipe) *Config {
	cfg := configDefault
	cfg.swDbVersion = swDbVersion
	cfg.initRecipe = initRecipe
	if swDbVersion > 0 {
		// We only need currentVersion-1 slots; but this will all be easier if
		// we use 1-offset.
		cfg.migrateRecipes = make([]*Recipe, swDbVersion)
	}
	return &cfg
}

// SetSchemaName sets the database schema name.  It is recommended this be of
// the form <packagepath>#<schemaname>; for example,
// "gitlab.com/martyros/languagedb#words".  If set, opened databases will check
// that it is equivalent when opening; if not set, opened databases will verify
// that no such key exists.
func (cfg *Config) SetSchemaName(schemaName string) *Config {
	if cfg.err != nil {
		return cfg
	}

	cfg.schemaName = schemaName

	return cfg
}

// SetMigrationRecipe configures how to upgrade from version vfrom to vfrom+1.
// vfrom must be strictly less than the swDbVersion passed to Schema().
func (cfg *Config) SetMigrationRecipe(vfrom int, recipe Recipe) *Config {
	if cfg.err != nil {
		return cfg
	}

	if vfrom >= cfg.swDbVersion {
		cfg.err = ErrorInvalidMigrationVersion.errorf("SetMigration: vfrom %d >= cfg.currentVersion %d",
			vfrom, cfg.swDbVersion)
		return cfg
	}

	cfg.migrateRecipes[vfrom] = &recipe

	return cfg
}

// SetParamTableName configures the name of the parameter table which contains the
// database version.  This can be used for other purposes, as long as the key
// "dbversion" is not used.  NB that this table name CANNOT BE MIGRATED, so
// choose it wisely.
//
// Default: "params"
func (cfg *Config) SetParamTableName(name string) *Config {
	if cfg.err != nil {
		return cfg
	}
	cfg.paramTableName = name
	return cfg
}

// SetAutoMigrate configures whether database schema migration will happen
// automatically or not. If false, an error will be returned if an old version
// of the database is opened.
//
// Default: false
func (cfg *Config) SetAutoMigrate(autoMigrate bool) *Config {
	if cfg.err != nil {
		return cfg
	}
	cfg.autoMigrate = autoMigrate
	return cfg
}

func (cfg *Config) getCurrentDatabaseVersion(tx sqlx.Ext) (int, error) {
	var versionString string
	if err := sqlx.Get(tx, &versionString,
		fmt.Sprintf(`select value from %s where key=?`, cfg.paramTableName),
		PARAMS_KEY_DBVERSION); err != nil {
		// Theoretically we should check that the error was that the table
		// didn't exist, rather than any other errors.  But looking at the
		// sqlite3 error codes, it wasn't clear which of those errors it would
		// be.
		return 0, nil
	}

	if version, err := strconv.Atoi(versionString); err != nil {
		return 0, ErrorInternal.errorf("Internal error: Cannot convert version string %s to an integer: %w", versionString, err)
	} else {
		return version, nil
	}
}

func (recipe *Recipe) apply(cfg *Config, eq sqlx.Ext) error {
	if recipe.Sql != "" {
		if _, err := eq.Exec(recipe.Sql); err != nil {
			return fmt.Errorf("Executing sql: %w", err)
		}
	}

	if recipe.Function != nil {
		if err := recipe.Function(cfg, eq); err != nil {
			return fmt.Errorf("Executing function: %w", err)
		}
	}

	return nil
}

// SetParam sets the key to the specified value (updating they key to the new
// value if it already exists).  It can be passed either a transaction or a
// plain database.
func (cfg *Config) SetParam(eq sqlx.Ext, param string, value string) error {
	if _, err := eq.Exec(fmt.Sprintf(
		`insert into %s (key, value) values (?, ?) 
             on conflict(key) do update set value=excluded.value;`,
		cfg.paramTableName),
		param, value); err != nil {
		return fmt.Errorf("Setting keyvalue: Upserting (%s, %s) into table %s: %w",
			param, value, cfg.paramTableName, err)
	}
	return nil
}

// GetParam gets the value of the specified key.  If the key doesn't exist,
// sql.ErrNoRows will be returned (perhaps wrapped).
func (cfg *Config) GetParam(eq sqlx.Ext, key string) (string, error) {
	var value string
	err := sqlx.Get(eq, &value,
		fmt.Sprintf(`select value from %s where key=?`, cfg.paramTableName),
		key)
	return value, err
}

func (cfg *Config) setDbVersion(eq sqlx.Ext) error {
	if err := cfg.SetParam(eq, PARAMS_KEY_DBVERSION, strconv.Itoa(cfg.swDbVersion)); err != nil {
		return fmt.Errorf("Setting DB version: %w", err)
	}
	return nil
}

func (cfg *Config) initDb(eq sqlx.Ext) error {
	// Obviously normally using Sprintf for a query is a terrible idea, but we
	// can't exactly use a "database parameter" as a table name...
	if _, err := eq.Exec(fmt.Sprintf(`create table %s (
		key       text primary key,
		value     text not null
	)`,
		cfg.paramTableName)); err != nil {
		return fmt.Errorf("Creating params table %s: %w", cfg.paramTableName, err)
	}

	if err := cfg.setDbVersion(eq); err != nil {
		return err
	}

	if cfg.schemaName != "" {
		if err := cfg.SetParam(eq, PARAMS_KEY_DBSCHEMANAME, cfg.schemaName); err != nil {
			return fmt.Errorf("Setting DB schema name: %w", err)
		}
	}

	if err := cfg.initRecipe.apply(cfg, eq); err != nil {
		return fmt.Errorf("Applying database initialization recipe: %w", err)
	}

	return nil
}

// Migrate to the newest version of the database.  Just set the new database
// version once after all upgrades have completed.
func (cfg *Config) migrate(dbVersion int, eq sqlx.Ext) error {
	for i := dbVersion; i < cfg.swDbVersion; i++ {
		recipe := cfg.migrateRecipes[i]
		if recipe == nil {
			return ErrorNoMigrationRecipe.errorf("No recipe to upgrade from version %d", i)
		}

		if err := recipe.apply(cfg, eq); err != nil {
			return ErrorMigrationFailed.errorf("Applying migration recipe from version %d: %w", i, err)
		}
	}

	return cfg.setDbVersion(eq)
}

func (cfg *Config) openTx(eq sqlx.Ext) error {
	version, err := cfg.getCurrentDatabaseVersion(eq)
	if err != nil {
		// Query error or unexpected value
		return err
	}

	if version != 0 {
		dbschemaname, err := cfg.GetParam(eq, PARAMS_KEY_DBSCHEMANAME)
		if cfg.schemaName == "" {
			if err == nil {
				return ErrorSchemaNameMismatch.errorf("Config has no schema name, but database has name %s", dbschemaname)
			}
			if !errors.Is(err, sql.ErrNoRows) {
				return err
			}
		} else {
			if errors.Is(err, sql.ErrNoRows) {
				return ErrorSchemaNameMismatch.errorf("Config has schema name %s but database has no schema name", cfg.schemaName)
			}
			if err != nil {
				return err
			}
			if cfg.schemaName != dbschemaname {
				return ErrorSchemaNameMismatch.errorf("SchemaName mismatch: Config %s Database %s",
					cfg.schemaName, dbschemaname)
			}
		}
	}

	switch {
	case version == cfg.swDbVersion:
		break
	case version == 0:
		// Database is uninitialized; initialize it
		err = cfg.initDb(eq)
	case version > cfg.swDbVersion:
		err = ErrDbTooNew.errorf("Database version %d newer than software version %d", version, cfg.swDbVersion)
	case version < cfg.swDbVersion:
		if cfg.autoMigrate {
			// We've got a version mismatch; try migrating the database to the
			// current version
			err = cfg.migrate(version, eq)
		} else {
			err = ErrorNeedsUpgrade.errorf("Database version %d, current version %d.  Please migrate.", version, cfg.swDbVersion)
		}
	default:
		err = fmt.Errorf("INTERNAL ERROR: Unexpected unhandled case")
	}

	return err
}

// OpenTx will do the same thing as open, but with an sqlx.Tx instead.
// (Alternately, if you want to do the open operation without a transaction, you
// can call this with the raw database, but that's not recommended.)
func (cfg *Config) OpenTx(eq sqlx.Ext) error {
	if cfg.err != nil {
		return cfg.err
	}

	return cfg.openTx(eq)
}

// Open will check whether the database has been initialized, and if so, what
// version it is.  If it cannot read the version, it will attempt to initialize
// the database with the newest schema version.
func (cfg *Config) Open(dbx *sqlx.DB) error {
	if cfg.err != nil {
		return cfg.err
	}

	if tx, err := dbx.Beginx(); err != nil {
		return fmt.Errorf("Starting transaction: %w", err)
	} else if err := cfg.openTx(tx); err != nil {
		tx.Rollback()
		return err
	} else if err := tx.Commit(); err != nil {
		return fmt.Errorf("Committing transaction: %w", err)
	} else {
		return nil
	}
}
