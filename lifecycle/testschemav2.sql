create table students (
    student_id integer primary key,
    firstname  text,
    lastname   text,
    DOB        date
);

create table departments (
    department_id    integer primary key,
    department_abbrv text,
    department_full  text
);

create table classes (
    class_id      integer primary key,
    department_id integer references departments,
    class_number  integer,
    title         text,
    descripiton   text,
    classtimes    text
);

create table studentclass (
    class_id integer references classes,
    student_id integer references students
);