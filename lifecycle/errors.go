package lifecycle

import "fmt"

type LifecycleError struct {
	errno int
	error
}

func (e LifecycleError) Is(target error) bool {
	if te, ok := target.(LifecycleError); !ok {
		return false
	} else {
		return te.errno == e.errno
	}
}

func (e LifecycleError) Unwrap() error {
	return e.error
}

func (e LifecycleError) errorf(format string, args ...any) LifecycleError {
	return LifecycleError{
		errno: e.errno,
		error: fmt.Errorf(format, args...),
	}
}

var (
	ErrorNeedsUpgrade            = LifecycleError{errno: 1}
	ErrorInvalidMigrationVersion = LifecycleError{errno: 2}
	ErrorInternal                = LifecycleError{errno: 3}
	ErrorNoMigrationRecipe       = LifecycleError{errno: 4}
	ErrorMigrationFailed         = LifecycleError{errno: 5}
	ErrorSchemaNameMismatch      = LifecycleError{errno: 6}
	ErrDbTooNew                  = LifecycleError{errno: 7}
)
