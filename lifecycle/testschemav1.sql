create table students (
    student_id integer primary key,
    firstname text,
    lastname text
);

create table classes (
    class_id integer primary key,
    title text,
    descripiton text,
    classtimes text
);

create table studentclass (
    class_id integer references classes,
    student_id integer references students
);