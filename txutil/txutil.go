// The txutil package implements utility functions relating to
// automatically retrying transactions.
package txutil

import (
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/mattn/go-sqlite3"

	"gitlab.com/martyros/sqlutil/liteutil"
)

// ShouldRetry takes an error and tries to determine if it's an error
// which should lead to a transaction retry, or a transaction failure.
// Currently only works for mattn/go-sqlite3 error codes.
func ShouldRetry(err error) bool {
	return liteutil.IsSqliteErrorCode(err, sqlite3.ErrBusy, sqlite3.ErrLocked)
}

func errOrRetry(comment string, err error) error {
	if ShouldRetry(err) {
		return err
	}
	return fmt.Errorf("%s: %v", comment, err)
}

// ErrorRollbackSuccess if returned from the TxLoopDb function, will cause the
// transaction will be rolled back, but 'nil' to be returned from the outer
// function.  This allows an easy way to rollback a transaction while having the
// outer operation not need any special logic.
var ErrorRollbackSuccess = errors.New("internal error: rollback-but-succeed")

// TxLoopDb implements the boilerplate around making transaction loops.
//
// The callback will be handed an interface suitable to do Query and Exec calls.
// Any error it gets from database calls should be passed up either as-is, or
// reachable by an appropriate number of Unwrap calls.
//
// TxLoopDb will start the transaction, and check for errors which should lead
// to a retry; those transactions will be re-started and the function called
// again.  An ErrorRollbackSuccess error will cause the transaction to be
// cancelled, but 'nil' to be returned to the top level. Any other error will
// cause the transaction to be cancelled and will be passed up unchanged.
//
// Callers should make sure that the function has no changes to long-term
// outside state, even if returing success, as the final commit may still fail.
//
// Example:
//
//	var FooType Foo
//
//	err := TxLoopDb(db, func(eq sqlx.Ext) error {
//	    _, err := eq.Exec(...)
//	   if err != nil {
//	       return fmt.Errorf("...: %w", err)
//	   }
//
//	   err := sqlx.Get(eq, &Foo, ...)
//	   if err != nil {
//	       return fmt.Errorf("...: %w", err)
//	   }
//
//	  return nil
//
//	}
//
//	// Use Foo only if TxLoopDb returned no error
func TxLoopDb(db *sqlx.DB, txFunc func(eq sqlx.Ext) error) error {
	for {
		tx, err := db.Beginx()
		if ShouldRetry(err) {
			continue
		} else if err != nil {
			return fmt.Errorf("Starting transaction: %w", err)
		}
		defer tx.Rollback()

		err = txFunc(tx)
		if ShouldRetry(err) {
			tx.Rollback()
			continue
		} else if err == ErrorRollbackSuccess {
			return nil
		} else if err != nil {
			return err
		}

		err = tx.Commit()
		if ShouldRetry(err) {
			tx.Rollback()
			continue
		} else if err != nil {
			err = fmt.Errorf("Commiting transaction: %w", err)
		}
		return err
	}
}
