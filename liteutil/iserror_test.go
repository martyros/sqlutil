package liteutil

import (
	"fmt"
	"testing"

	"github.com/mattn/go-sqlite3"
)

func TestUnitIsSqliteError(t *testing.T) {
	tests := []struct {
		err     error
		queries []error
		want    bool
	}{
		{err: sqlite3.Error{Code: sqlite3.ErrBusy},
			queries: []error{sqlite3.ErrBusy},
			want:    true},
		{err: sqlite3.Error{Code: sqlite3.ErrBusy},
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrBusy},
			want:    true},
		{err: sqlite3.Error{Code: sqlite3.ErrInternal},
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrBusy},
			want:    false},
		{err: sqlite3.Error{Code: sqlite3.ErrIoErr, ExtendedCode: sqlite3.ErrIoErrWrite},
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrIoErrWrite},
			want:    true},
		{err: fmt.Errorf("A wrapped error: %w",
			sqlite3.Error{Code: sqlite3.ErrIoErr, ExtendedCode: sqlite3.ErrIoErrWrite}),
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrIoErrWrite},
			want:    true},
		{err: fmt.Errorf("A wrapped error: %w",
			sqlite3.Error{Code: sqlite3.ErrLocked, ExtendedCode: sqlite3.ErrLockedSharedCache}),
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrLockedSharedCache},
			want:    true},
		{err: fmt.Errorf("A wrapped error: %w",
			sqlite3.Error{Code: sqlite3.ErrLocked, ExtendedCode: sqlite3.ErrLockedSharedCache}),
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrLocked},
			want:    true},
		{err: fmt.Errorf("A wrapped error: %w",
			sqlite3.Error{Code: sqlite3.ErrLocked, ExtendedCode: sqlite3.ErrLockedSharedCache}),
			queries: []error{sqlite3.ErrAbort, sqlite3.ErrIoErr},
			want:    false},
		{err: sqlite3.Error{Code: sqlite3.ErrIoErr, ExtendedCode: sqlite3.ErrIoErrWrite},
			queries: []error{sqlite3.ErrAbort, fmt.Errorf("Incorrect error"), sqlite3.ErrIoErr},
			want:    true},
		{err: nil,
			queries: []error{sqlite3.ErrAbort, fmt.Errorf("Incorrect error"), sqlite3.ErrIoErr},
			want:    false},
		{err: fmt.Errorf("A random error"),
			queries: []error{sqlite3.ErrAbort, fmt.Errorf("Incorrect error"), sqlite3.ErrIoErr},
			want:    false},
	}

	for _, test := range tests {
		got := IsSqliteErrorCode(test.err, test.queries...)
		if test.want != got {
			t.Errorf("Failed test %v: got %v!", test, got)
		}
	}
}
