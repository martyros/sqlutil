// The liteutil package implements utility functions around mattn's
// go-sqlite3 package
package liteutil

import (
	"errors"

	"github.com/mattn/go-sqlite3"
)

// sqlite3 error code definitiions are somewhat difficult to use: You
// can't simply compare the Err* constants to the resulting value,
// because they're of different types.
//
// IsSqliteErrorCode will take an error and a range of "query errors"
// (which must be of type sqlite3.ErrNo or sqlite3.ErrNoExtended) and
// check for matches.  (Values not of these types will be skipped.)
//
// For example, the following will check to see if `err` is (or
// contains) ErrBusy or ErrLocked:
//
//     IsSqliteErrorCode(err, sqlite3.ErrBusy, sqlite3.ErrLocked)
//
func IsSqliteErrorCode(err error, queries ...error) bool {
	if err == nil {
		return false
	}

	var sqliteErr sqlite3.Error

	// Progressively Unwrap errors until we find one of type sqlite3.Error
	for {
		var ok bool
		sqliteErr, ok = err.(sqlite3.Error)
		if ok {
			break
		}
		err = errors.Unwrap(err)
		if err == nil {
			return false
		}
	}

	for _, qerr := range queries {
		switch v := qerr.(type) {
		case sqlite3.ErrNo:
			if sqliteErr.Code == v {
				return true
			}
		case sqlite3.ErrNoExtended:
			if sqliteErr.ExtendedCode == v {
				return true
			}
		}
		// TODO: This could be extended to also handle syscall.Errno types
	}

	return false
}

// IsErrorConstraintUnique will return true for either
// ErrConstraintUnique or ErrConstraintPrimaryKey.
func IsErrorConstraintUnique(err error) bool {
	return IsSqliteErrorCode(err, sqlite3.ErrConstraintUnique, sqlite3.ErrConstraintPrimaryKey)
}

// IsErrorForeignKey will return true for ErrConstraintForeignKey
func IsErrorForeignKey(err error) bool {
	return IsSqliteErrorCode(err, sqlite3.ErrConstraintForeignKey)
}
