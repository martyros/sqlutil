// Package timedb provides wrappers for the standard library time.Location and
// time.Time which implement database/sql Scanner and Value interfaces; meaning
// they can be passed directly to database drivers and correct parsing will be
// done.
//
// It also provides some "wrapper" functions to avoid awkward conversions.
package timedb

import (
	"database/sql/driver"
	"fmt"
	"time"
)

// Location is database-scannable wrapper around time.Location
type Location struct {
	*time.Location
}

func (l *Location) Scan(src interface{}) error {
	switch ts := src.(type) {
	case string:
		var err error
		l.Location, err = time.LoadLocation(ts)
		if err != nil {
			return fmt.Errorf("LocationScan: LoadLocation for %s returned %v",
				ts, err)
		}
		return nil
	default:
		return fmt.Errorf("LocationScan: Cannot convert type %t into Location", src)
	}
}

func (l Location) Value() (driver.Value, error) {
	return driver.Value(l.String()), nil
}

// Time is a database-scannable wrapper around time.Time
type Time struct {
	time.Time
}

// NewTime takes a stdlib time.Time type and returns a timedb.Time
// type
func NewTime(t time.Time) Time {
	return Time{Time: t}
}

func (t *Time) Scan(src interface{}) error {
	var ts []byte

	switch v := src.(type) {
	case string:
		if v == "" {
			return fmt.Errorf("Invalid time [empty string]")
		}
		ts = []byte(v)
	case []byte:
		ts = v
	default:
		return fmt.Errorf("TimeScan: Cannot convert type %t into Time", src)
	}

	if err := t.UnmarshalText(ts); err != nil {
		return fmt.Errorf("TimeScan: UnmarshalText returned for %s returned %v",
			string(ts), err)
	}
	return nil
}

func (t Time) Value() (driver.Value, error) {
	text, err := t.MarshalText()
	return driver.Value(string(text)), err
}
