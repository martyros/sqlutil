package timedb

import "time"

// LoadLocation is a convenience wrapper around time.LoadLocation; it returns a
// timedb.Location containing the results of the corresponding time.Location
// call.
func LoadLocation(name string) (Location, error) {
	l, err := time.LoadLocation(name)
	return Location{Location: l}, err
}

// Date is a convenience wrapper around time.Date; it returns a timedb.Time
// containing the results of the corresponding time.Date call.
func Date(year int, month time.Month, day, hour, min, sec, nsec int, loc *time.Location) Time {
	return Time{Time: time.Date(year, month, day, hour, min, sec, nsec, loc)}
}

func Now() Time {
	return Time{Time: time.Now()}
}

// ParseInLocation is a convenience wrapper around time.ParseInLocation; it
// takes a timedb.Location type, and returns a timedb.Time containing the
// results of the corresponding time.ParseInLocation call.
func ParseInLocation(layout, value string, loc Location) (Time, error) {
	t, err := time.ParseInLocation(layout, value, loc.Location)
	return Time{Time: t}, err
}

// AddDate is a convenience wrapper for time.Time.AddDate; it takes a
// timedb.Time and returns a timedb.Time corresponding to the time.Time.AddDate
// call.
func (t Time) AddDate(years int, months int, days int) Time {
	return Time{Time: t.Time.AddDate(years, months, days)}
}

// In is a convenience wrapper for time.Time.In; it takes a timedb.Time and a
// timedb.Location and returns a timedb.Time corresponding to the time.Time.In
// call.
func (t Time) In(loc *Location) Time {
	return Time{Time: t.Time.In(loc.Location)}
}
