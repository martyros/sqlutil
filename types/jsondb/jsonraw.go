package jsondb

import "encoding/json"

// Json holds a byte slice that contains already-Marshalled JSON.  It implements
// json.Marshaler and json.Unmarhaler, so that it can be included in larger
// structures which will be marshalled to JSON.
type Json []byte

func (rj Json) MarshalJSON() ([]byte, error) {
	return []byte(rj), nil
}

func (rj *Json) UnmarshalJSON(in []byte) error {
	*rj = Json(in)
	return nil
}

func (rj *Json) Marshal(v any) error {
	b, err := json.Marshal(v)
	if err == nil {
		*rj = Json(b)
	}
	return err
}

func (rj Json) String() string {
	return string(rj)
}

func (rj Json) Unmarshal(v any) error {
	return json.Unmarshal([]byte(rj), v)
}

func Marshal(v any) (Json, error) {
	b, err := json.Marshal(v)
	return Json(b), err
}
