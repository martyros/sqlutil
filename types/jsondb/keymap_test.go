package jsondb_test

import (
	"database/sql"
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/martyros/sqlutil/types/jsondb"
)

func TestKeyMap(t *testing.T) {
	db, err := sqlx.Open("sqlite3", "file::memory:?cache=shared&_foreign_keys=on")
	//db, err := sqlx.Open("sqlite3", "file:keymaptest.sqlite?cache=shared&_foreign_keys=on")
	if err != nil {
		t.Errorf("ERROR Opening in-memory database for testing: %v", err)
		return
	}

	if _, err := db.Exec(`create table keymap_test(idx integer primary key, val text);`); err != nil {
		t.Errorf("ERROR Creating test table: %v", err)
		return
	}

	in := make([]jsondb.KeyMap, 4)
	in[0].Set("a", []string{"a", "b", "c"})
	// [1] left nil
	in[2].Set("a", "b")
	in[3].Set("a", "b")
	in[3].Set("c", "d")

	for i, s := range in {
		if _, err := db.Exec(`insert into keymap_test(idx, val) values (?, ?)`,
			i, s); err != nil {
			t.Errorf("ERROR Inserting (%d,%v) into table: %v", i, s, err)
		}

		// Check to see that any empty values are inserted as NULL
		if s == nil {
			var str sql.NullString
			if err := db.Get(&str, `select val from keymap_test where idx=?`, i); err != nil {
				t.Errorf("ERROR Getting value just written")
			} else {
				if str.Valid {
					t.Errorf("ERROR: Expected NULL value in databse for %v, got %v!", s, str)
				}
			}
		} else {

			if i == 0 {
				k, v := "a[1]", "b"
				var str string
				if err := db.Get(&str,
					"select json_extract(val, '$.a[1]') from keymap_test where idx=?", i); err != nil {
					t.Errorf("ERROR idx %d Getting value for key %s: %v", i, k, err)
				} else if str != v {
					t.Errorf("ERROR idx %d key %s, wanted value %s got %s!", i, k, v, str)
				}
			} else {
				// Check to see that the json is extract-able via SQLITE's built-in json function
				for k, v := range in[i] {
					var strval string
					if err := v.Unmarshal(&strval); err != nil {
						t.Errorf("Unmarshaling key %v into a string: %v", v, err)
						continue
					}
					var str string
					if err := db.Get(&str,
						fmt.Sprintf("select json_extract(val, '$.%s') from keymap_test where idx=?", k), i); err != nil {
						t.Errorf("ERROR idx %d Getting value for key %s: %v", i, k, err)
					} else if str != strval {
						t.Errorf("ERROR idx %d key %s, wanted value %s got %s!", i, k, v, str)
					}
				}
			}
		}
	}

	var out []jsondb.KeyMap

	if err := db.Select(&out, `select val from keymap_test order by idx`); err != nil {
		t.Errorf("ERROR Getting values out as an array: %v", err)
	} else if !cmp.Equal(in, out) {
		t.Errorf("ERROR Wanted %v got %v (diff %v)", in, out, cmp.Diff(in, out))
	}

	var valstrarr []string
	if err, _ := out[0].Get("a", &valstrarr); err != nil {
		t.Errorf("Getting out[0].a: %v", err)
	} else if !cmp.Equal(valstrarr, []string{"a", "b", "c"}) {
		t.Errorf("Unexpected string array value: wanteg ['a', 'b', 'c'], got %v", valstrarr)
	}

	if out[1] != nil {
		t.Errorf("Expected out[1] to be nil, got %v", out[1])
	}

	var valstr string
	if err, _ := out[2].Get("a", &valstr); err != nil {
		t.Errorf("Getting out[2].a: %v", err)
	} else if valstr != "b" {
		t.Errorf("Unexpected string value: wanted b, got %s!", valstr)
	}

	if _, prs := out[2].Get("c", &valstr); prs {
		t.Error("Unexpectedly-present value out[2].c!")
	}

	if err, _ := out[3].Get("a", &valstr); err != nil {
		t.Errorf("Getting out[3].a: %v", err)
	} else if valstr != "b" {
		t.Errorf("Unexpected string value: wanted b, got %s!", valstr)
	}

	if err, _ := out[3].Get("c", &valstr); err != nil {
		t.Errorf("Getting out[3].c: %v", err)
	} else if valstr != "d" {
		t.Errorf("Unexpected string value: wanted d, got %s!", valstr)
	}

}
