package jsondb_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/martyros/sqlutil/types/jsondb"
)

func TestMarshal(t *testing.T) {
	var x, y, z struct {
		A int
		B float64
		C string
	}

	x.A = 5
	x.B = 3.14
	x.C = "Pumpkin pie"

	xj, err := jsondb.Marshal(x)
	if err != nil {
		t.Fatalf("Marshaling struct: %v", err)
	}

	xjb, err := json.Marshal(xj)
	if err != nil {
		t.Fatalf("Marshaling xj: %v", err)
	}

	err = xj.Unmarshal(&y)
	if err != nil {
		t.Fatalf("Unmarhaling xj: %v", err)
	}

	var zj jsondb.Json

	err = json.Unmarshal(xjb, &zj)
	if err != nil {
		t.Fatalf("Unmarshaling xjb into zj: %v", err)
	}

	err = zj.Unmarshal(&z)
	if err != nil {
		t.Fatalf("Unmarshaling zj into z: %v", err)
	}

	if y != x {
		t.Errorf("y %v != x %v", y, x)
	}

	if z != x {
		t.Errorf("z %v != x %v", z, x)
	}

	z.A = 6
	err = zj.Marshal(z)
	if err != nil {
		t.Fatalf("Marshaling z into zj: %v", err)
	}

	err = zj.Unmarshal(&y)
	if err != nil {
		t.Fatalf("Unmarshaling zj into y: %v", err)
	}

	if z != y {
		t.Errorf("z %v != y %v!", z, y)
	}
}
