package jsondb

import (
	"fmt"
	"reflect"
	"sync"
)

// SchemaRegistry is used to enforce a schema on the content of JSON keys.
type SchemaRegistry struct {
	fields map[string]reflect.Type
	mu     sync.RWMutex
}

func NewSchemaRegistry() *SchemaRegistry {
	return &SchemaRegistry{
		fields: make(map[string]reflect.Type),
	}
}

// Register the given key as being of the given type.
func (sr *SchemaRegistry) Register(key string, field any) error {
	sr.mu.Lock()
	defer sr.mu.Unlock()
	if _, ok := sr.fields[key]; ok {
		return fmt.Errorf("key %s is already registered", key)
	}
	sr.fields[key] = reflect.TypeOf(field)
	return nil
}

func (sr *SchemaRegistry) validateType(key string, value interface{}) error {
	sr.mu.RLock()
	expectedType, ok := sr.fields[key]
	sr.mu.RUnlock()

	if !ok {
		return fmt.Errorf("key %s is not registered in the schema", key)
	}

	actualType := reflect.TypeOf(value)
	if actualType != expectedType {
		return fmt.Errorf("invalid type for key %s: expected %v, got %v", key, expectedType, actualType)
	}

	return nil
}

// Use generics to enable static type checking

// SetJsonKeyMap will attempt to set the value of the key in the key map to the
// given value. Will fail if the given key is not registered in the schema
// registry, or if the key is registered to a different type.
func SetJsonKeyMap[T any](km *KeyMap, reg *SchemaRegistry, key string, value T) error {
	if err := reg.validateType(key, value); err != nil {
		return err
	}
	return km.Set(key, value)
}

// GetJsonKeyMap will attempt to get the value of the key from the keymap in the
// form of the given type. Will fail if the given key is not registered, or if
// the key is registered to a different type.
func GetJsonKeyMap[T any](km *KeyMap, reg *SchemaRegistry, key string) (T, error, bool) {
	var t T
	if err := reg.validateType(key, t); err != nil {
		return t, err, false
	}
	err, prs := km.Get(key, &t)
	return t, err, prs
}
