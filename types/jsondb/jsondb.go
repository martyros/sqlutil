// Package jsondb implements some structures useful for marshaling data
// structures into and out of a database.
package jsondb

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

// KeyMap is a structure designed to allow key-value pairs to be stored in a
// JSON field in a database; and speficially, to allow flexible recursive JSON
// structures if desired.
type KeyMap map[string]Json

func (km *KeyMap) Scan(src interface{}) error {
	switch ts := src.(type) {
	case string:
		*km = make(map[string]Json)
		if err := json.Unmarshal([]byte(ts), km); err != nil {
			return fmt.Errorf("Unmarshalling json: %w", err)
		}
		return nil
	case nil:
		*km = KeyMap(nil)
		return nil
	default:
		return fmt.Errorf("KeyMap.Scan: Got source type %t, expected string or NULL", src)
	}
}

func (km KeyMap) Value() (driver.Value, error) {
	if km == nil {
		return driver.Value(nil), nil
	} else if b, err := json.Marshal(km); err != nil {
		return nil, err
	} else {
		return driver.Value(string(b)), nil
	}
}

// Get unmarshals the Json contained in km[key] into val.  If km is nil, or the key
// does not exist, val is left empty and the bool is set to 'false'.  Otherwise,
// the bool returns 'true', and error is the error value from marshalling.
func (km KeyMap) Get(key string, val any) (error, bool) {
	if km == nil {
		return nil, false
	}
	if valj, prs := km[key]; prs {
		return valj.Unmarshal(val), true
	}
	return nil, false
}

// Set marshal the key val into km[key].  If km is nil, it is initialized.
func (km *KeyMap) Set(key string, val any) error {
	if *km == nil {
		*km = make(KeyMap)
	}
	valj, err := Marshal(val)
	if err == nil {
		(*km)[key] = valj
	}
	return err
}
