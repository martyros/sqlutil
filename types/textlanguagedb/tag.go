// Package textlanguagedb provides wrappers for golang.org/x/text/language
// package language.Script which implement database/sql Scanner and Value
// interfaces; meaning they can be passed directly to database drivers and
// correct parsing will be done.
//
// It also provides some "wrapper" functions to avoid awkward conversions.
package textlanguagedb

import (
	"database/sql/driver"
	"fmt"

	"golang.org/x/text/language"
)

type Tag struct {
	language.Tag
}

func NewTag(t language.Tag) Tag {
	return Tag{t}
}

func (t *Tag) Scan(src interface{}) error {
	*t = Tag{}
	switch ts := src.(type) {
	case string:
		var err error
		err = t.UnmarshalText([]byte(ts))
		if err != nil {
			return fmt.Errorf("TagScan: UnmarshalText for %s returned %v",
				ts, err)
		}
		return nil
	case nil:
		return nil
	default:
		return fmt.Errorf("TagScan: Cannot convert type %t into Tag", src)
	}
}

func (t Tag) Value() (driver.Value, error) {
	if t.IsRoot() {
		return driver.Value(nil), nil
	} else if b, err := t.MarshalText(); err != nil {
		return nil, err
	} else {
		return driver.Value(string(b)), nil
	}
}

func MustParse(t string) Tag {
	return Tag{language.MustParse(t)}
}

func Parse(ts string) (Tag, error) {
	if t, err := language.Parse(ts); err != nil {
		return Tag{}, err
	} else {
		return Tag{t}, nil
	}
}
