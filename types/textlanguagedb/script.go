// Package textlanguagedb provides wrappers for golang.org/x/text/language
// package language.Script which implement database/sql Scanner and Value
// interfaces; meaning they can be passed directly to database drivers and
// correct parsing will be done.
//
// It also provides some "wrapper" functions to avoid awkward conversions.
package textlanguagedb

import (
	"database/sql/driver"
	"fmt"

	"golang.org/x/text/language"
)

type Script struct {
	language.Script
}

func NewScript(scr language.Script) Script {
	return Script{scr}
}

func (s *Script) Scan(src interface{}) error {
	switch ts := src.(type) {
	case string:
		var err error
		s.Script, err = language.ParseScript(ts)
		if err != nil {
			return fmt.Errorf("ScriptScan: ParseScript for %s returned %v",
				ts, err)
		}
		return nil
	case nil:
		*s = Script{}
		return nil
	default:
		return fmt.Errorf("ScriptScan: Cannot convert type %t into Script", src)
	}
}

func (s Script) Value() (driver.Value, error) {
	if s.IsUnspecified() {
		return driver.Value(nil), nil
	} else {
		return driver.Value(s.String()), nil
	}
}

// IsUnspecified will return whether the underlying Script in question is
// unspecified, by checking whether s.String() == "Zzzz" (as specified by the
// language package)
func (s Script) IsUnspecified() bool {
	return s.String() == "Zzzz"
}

func MustParseScript(s string) Script {
	return Script{language.MustParseScript(s)}
}

func ParseScript(s string) (Script, error) {
	if scr, err := language.ParseScript(s); err != nil {
		return Script{}, err
	} else {
		return Script{scr}, nil
	}
}
