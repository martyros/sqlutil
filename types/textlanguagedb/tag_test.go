package textlanguagedb_test

import (
	"database/sql"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/martyros/sqlutil/types/textlanguagedb"
)

func TestTag(t *testing.T) {
	db, err := sqlx.Open("sqlite3", "file::memory:?cache=shared&_foreign_keys=on")
	if err != nil {
		t.Errorf("ERROR Opening in-memory database for testing: %v", err)
		return
	}

	if _, err := db.Exec(`create table tag_test(idx integer primary key, val text);`); err != nil {
		t.Errorf("ERROR Creating test table: %v", err)
		return
	}

	in := []textlanguagedb.Tag{
		{},
		textlanguagedb.MustParse("cmn-Hans"),
		textlanguagedb.MustParse("cmn-Hant"),
		textlanguagedb.MustParse("cmn-pinyin"),
	}

	if !in[0].IsRoot() {
		t.Errorf("ERROR: Expected index 0, default value (%v) to be unspecified", in[0])
	}

	if in[1].IsRoot() {
		t.Errorf("ERROR: Expected index 1, value %v, to be specified", in[1])
	}

	for i, s := range in {
		if _, err := db.Exec(`insert into tag_test(idx, val) values (?, ?)`,
			i, s); err != nil {
			t.Errorf("ERROR Inserting (%d,%v) into table: %v", i, s, err)
		}

		// Check to see that any empty values are inserted as NULL
		if s.IsRoot() {
			var str sql.NullString
			if err := db.Get(&str, `select val from tag_test where idx=?`, i); err != nil {
				t.Errorf("ERROR Getting value just written")
			} else {
				if str.Valid {
					t.Errorf("ERROR: Expected NULL value in databse for %v, got %v!", s, str)
				}
			}
		}
	}

	var out []textlanguagedb.Tag

	if err := db.Select(&out, `select val from tag_test order by idx`); err != nil {
		t.Errorf("ERROR Getting values out as an array: %v", err)
	} else {
		for i := range in {
			if i > len(out) {
				t.Errorf("ERROR: Failed to get all values back out: wanted %d, got %d!",
					len(in), len(out))
				break
			}

			if in[i] != out[i] {
				t.Errorf("ERROR: Index %d wanted %v got %v", i, in, out)
			}
		}

		if len(out) > len(in) {
			t.Errorf("ERROR: Got more out than we put in: %v", out[len(in):])
		}
	}

}
