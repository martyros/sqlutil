module gitlab.com/martyros/sqlutil

go 1.18

require (
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/google/go-cmp v0.5.9
	github.com/icrowley/fake v0.0.0-20221112152111-d7b7e2276db2
	github.com/jmoiron/sqlx v1.3.5
	github.com/mattn/go-sqlite3 v1.14.14
	golang.org/x/crypto v0.8.0
	golang.org/x/exp v0.0.0-20230817173708-d852ddb80c63
	golang.org/x/text v0.9.0
)

require (
	github.com/corpix/uarand v0.0.0-20170723150923-031be390f409 // indirect
	github.com/lib/pq v1.10.6 // indirect
)
