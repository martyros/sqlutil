package guid_test

import (
	"encoding/json"
	"math/rand/v2"
	"reflect"
	"sort"
	"testing"
	"time"

	"gitlab.com/martyros/sqlutil/guid"
	"golang.org/x/exp/slices"
)

func TestGuid(t *testing.T) {
	// Make several copies of content, at least one second apart.  They should
	// retain sorting order.

	titles := []string{"你好", "How to train your dragon", "错错错"}

	var guids []guid.Guid

	for i := 0; i < 2; i++ {
		for _, title := range titles {
			guidgen := guid.NewGuidGenerator().
				Content(title).
				Content(23)

			if i > 0 {
				t.Logf("Including previous guid %v", guids[i-1])
				guidgen.Content(guids[i-1])
			}

			guid, err := guidgen.Guid()
			if err != nil {
				t.Errorf("ERROR: Calcuating value for title %s: %v", title, err)
			} else {
				t.Logf("Title %s GUID %v", title, guid)
				guids = append(guids, guid)
			}

			if guid2, err := guidgen.Guid(); err != nil {
				t.Errorf("Second read of value failed: %v", err)
			} else if guid != guid2 {
				t.Errorf("Second read of value got %v, expected %v!",
					guid, guid)
			}
			t.Log("Waiting for the next second...")
			time.Sleep(1 * time.Second)
		}
	}

	testGuidSortLess(t, guids)

	testGuidSortCompare(t, guids)

	testGuidMarshalText(t, guids)

	testGuidMarshalJson(t, guids)
}

func testGuidSortLess(t *testing.T, guids []guid.Guid) {
	t.Log("Shuffling and seeing if they re-sort (with sort.Slice(guid.Less)) to the same order")

	var unsort []guid.Guid

	unsort = append(unsort, guids...)

	rand.Shuffle(len(unsort), func(i, j int) {
		t := unsort[i]
		unsort[i] = unsort[j]
		unsort[j] = t
	})

	t.Logf("Shuffled order: %v", unsort)

	sort.Slice(unsort, func(i, j int) bool {
		return unsort[i].Less(unsort[j])
	})

	t.Logf("Sorted order: %v", unsort)

	if !reflect.DeepEqual(guids, unsort) {
		t.Errorf("Unexpected resort: Got %v wanted %v",
			unsort, guids)
	}
}

func testGuidSortCompare(t *testing.T, guids []guid.Guid) {
	t.Log("Shuffling and seeing if they re-sort (with slices.Sort(guid.Compare)) to the same order")

	var unsort []guid.Guid

	unsort = append(unsort, guids...)

	rand.Shuffle(len(unsort), func(i, j int) {
		t := unsort[i]
		unsort[i] = unsort[j]
		unsort[j] = t
	})

	t.Logf("Shuffled order: %v", unsort)

	slices.SortFunc(unsort, func(i, j guid.Guid) int {
		return i.Compare(j)
	})

	t.Logf("Sorted order: %v", unsort)

	if !reflect.DeepEqual(guids, unsort) {
		t.Errorf("Unexpected resort: Got %v wanted %v",
			unsort, guids)
	}
}

func testGuidMarshalText(t *testing.T, guids []guid.Guid) {
	t.Log("Testing MarshalText")

	if s, err := guid.NullGuid.MarshalText(); err != nil {
		t.Errorf("Error marshalling NullGuid: %v", err)
	} else if string(s) != "0" {
		t.Errorf("Unexpected marshal value for NullGuid: %s", s)
	}

	guidtexts := make([]string, len(guids))
	for i := range guids {
		b, err := guids[i].MarshalText()
		if err != nil {
			t.Errorf("Converting guid %x to text: %v", guids[i], err)
		} else {
			guidtexts[i] = string(b)
		}
	}

	t.Logf("Marshalled text: %v", guidtexts)

	t.Logf("Unmarshalling text")
	umguids := make([]guid.Guid, len(guids))

	for i := range guidtexts {
		err := umguids[i].UnmarshalText([]byte(guidtexts[i]))
		if err != nil {
			t.Errorf("Failed to unmarshal %s: %v",
				guidtexts[i], err)
		}
	}

	t.Logf("Checking equivalence")
	if !reflect.DeepEqual(guids, umguids) {
		t.Errorf("Unexpected resort: Got %v wanted %v",
			umguids, guids)
	}

	// TODO: Test unmarshalling of invalid guids (extected to fail)
}

func testGuidMarshalJson(t *testing.T, guids []guid.Guid) {
	t.Log("Testing MarshalText")

	guidjson, err := json.Marshal(guids)
	if err != nil {
		t.Errorf("ERROR marshalling json: %v", err)
		return
	}

	t.Logf("Marshalled JSON: %v", string(guidjson))

	t.Logf("Unmarshalling JSON")
	var umguids []guid.Guid
	err = json.Unmarshal(guidjson, &umguids)

	if err != nil {
		t.Errorf("ERROR unmarshalling json: %v", err)
		return
	}

	t.Logf("Checking equivalence")
	if !reflect.DeepEqual(guids, umguids) {
		t.Errorf("Unexpected resort: Got %v wanted %v",
			umguids, guids)
	}

	// TODO: Test unmarshalling of invalid guids (extected to fail)
}
