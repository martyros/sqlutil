package guid

import (
	"fmt"
	"os"
	"reflect"
	"sort"
	"testing"

	"github.com/jmoiron/sqlx"

	_ "github.com/mattn/go-sqlite3"
)

var sqlTestSetup = `
create table books (
    guid integer primary key,
    booktitle text,
    bookauthor text,
    booktext text
)`

type Book struct {
	Guid       Guid
	BookTitle  string
	BookAuthor string
	BookText   string
}

func TestSql(t *testing.T) {
	var db *sqlx.DB

	if dbFile, err := os.CreateTemp("", "guid*.sqlite"); err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	} else if tdb, err := sqlx.Open("sqlite3", fmt.Sprintf("file:%s", dbFile.Name())); err != nil {
		t.Errorf("Opening file %s as an sqlite database: %v",
			dbFile.Name(), err)
		return
	} else {
		t.Logf("Opened temp database file %s", dbFile.Name())
		db = tdb
	}

	t.Logf("About to execute sql %s", sqlTestSetup)
	if _, err := db.Exec(sqlTestSetup); err != nil {
		t.Errorf("Setting up tables: %v", err)
		return
	}

	tests := []Book{
		{BookTitle: "How to train your dragon",
			BookAuthor: "Not sure",
			BookText:   "<insert text here>"},
		{BookTitle: "How NOT to train your dragon",
			BookAuthor: "The other guy",
			BookText:   "<insert text there>"},
		{BookTitle: "How to encourage your dragon",
			BookAuthor: "Newt Skemander",
			BookText:   "<Insert salamander eyes here>"},
	}

	t.Logf("Generating GUIDs")
	for i := range tests {
		var err error
		tests[i].Guid, err =
			NewGuidGenerator().
				Content(tests[i].BookTitle).
				Content(tests[i].BookAuthor).
				Content(tests[i].BookText).
				Guid()
		if err != nil {
			t.Errorf("Error generating GUID: %v", err)
			return
		}
	}

	// Test the high-bit marshalling / unmarshalling
	tests[0].Guid.guid |= (1 << 62)

	t.Logf("Inserting structures into database")
	for i := range tests {
		_, err := db.Exec(`
			insert into books(guid, booktitle, bookauthor, booktext) 
			    values(?, ?, ?, ?)`,
			tests[i].Guid,
			tests[i].BookTitle,
			tests[i].BookAuthor,
			tests[i].BookText)
		if err != nil {
			t.Errorf("Inserting: %v", err)
		}
	}

	sort.Slice(tests, func(i, j int) bool {
		return tests[i].Guid.Less(tests[j].Guid)
	})

	t.Log("Reading books back from the database")
	var readbooks []Book
	if err := db.Select(&readbooks, `select * from books`); err != nil {
		t.Errorf("Reading back books: %v", err)
	} else {
		if len(readbooks) != len(tests) {
			t.Errorf("Unexpected number of books read back: got %d wanted %d",
				len(readbooks), len(tests))
		}

		sort.Slice(readbooks, func(i, j int) bool {
			return readbooks[i].Guid.Less(readbooks[j].Guid)
		})

		if !reflect.DeepEqual(tests, readbooks) {
			t.Errorf("Deep equal for %x and %x failed",
				tests, readbooks)
		}
	}

}
