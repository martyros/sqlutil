package guid

import (
	"fmt"
	"testing"
)

func TestErrors(t *testing.T) {
	// Content after an error should result in an error
	{
		guid, err := NewGuidGenerator().
			setError(fmt.Errorf("Testing")).
			Content("Hi").
			Guid()
		if err == nil {
			t.Errorf("Expected failure, got success (guid %x)", guid)
		}
	}

	// Adding content after calculatin ga value returns an error
	{
		g := NewGuidGenerator().
			Content("Hi")

		_, err := g.Guid()

		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		} else {
			g.Content("World")
			_, err := g.Guid()
			if err == nil {
				t.Errorf("Content-after-evaluate succeeded!")
			}
		}
	}
}
