package guid

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strconv"
)

// Guid implements the following interfaces:
// * database/sql/driver.Valuer
// * databse/sql.Scanner
// * encoding.TextMarshaler
// * encoding.TextUnmarshaler
// * encoding/json.Marshaler
// * encoding/json.Unmarshaler
//
// The Scanner / Valuer interpret it as an int64, allowing it to be used as a
// primary key.
//
// The Marshaler interfaces interpret it as a hexadecimal string, making it
// somewhat easier to deal with when observing text manually.
type Guid struct {
	guid int64
}

func (g Guid) Equal(h Guid) bool {
	return g.guid == h.guid
}

var NullGuid = Guid{guid: 0}

func (g *Guid) IsNull() bool {
	return g.guid == 0
}

// Less returns true if i < j
func (g Guid) Less(j Guid) bool {
	return g.guid < j.guid
}

// Compare returns the following values:
// a < b : -1
// a == b:  0
// a > b :  1
func (g Guid) Compare(j Guid) int {
	if g.guid < j.guid {
		return -1
	} else if g.guid > j.guid {
		return 1
	}
	return 0
}

func (g Guid) String() string {
	b, _ := g.MarshalText()
	return string(b)
}

func (g Guid) MarshalText() ([]byte, error) {
	//	return []byte(fmt.Sprintf("%016x", g.guid)), nil
	return []byte(strconv.FormatUint(uint64(g.guid), 16)), nil
}

func (g *Guid) UnmarshalText(text []byte) error {
	a, err := strconv.ParseUint(string(text), 16, 64)
	if err == nil {
		g.guid = int64(a)
	}
	return err
}

func (g Guid) MarshalJSON() ([]byte, error) {
	b, err := g.MarshalText()
	if err != nil {
		return nil, err
	}
	return json.Marshal(string(b))
}

func (g *Guid) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	return g.UnmarshalText([]byte(s))
}

func (g Guid) Value() (driver.Value, error) {
	if g == NullGuid {
		return nil, nil
	} else {
		return g.guid, nil
	}
}

func (g *Guid) Scan(src interface{}) error {
	switch v := src.(type) {
	case int64:
		g.guid = v
	case nil:
		*g = NullGuid
	default:
		return fmt.Errorf("Unexpected type for Guid: %v", src)
	}
	return nil
}
