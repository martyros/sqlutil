// Package guid implements a 64-bit global unique k-sortable ID suitable for use
// as the primary key of a database where entities may be generated on different
// hosts and merged later.  The uid has 31 bits of a timestamp and 32 bits of a
// hashed value of a pseudorandom number and the content of the entity.
//
// General usage would look like this, (with field_1 and field_2 being two
// different aspects of the content, such as Title and Text):
//
//	guid, err := guid.NewGuidGenerator().
//	    Content(field_1).
//	    Content(field_2).
//	    Guid()
//
// The resulting guid implements both sql.Scanner and driver.Valuer interfaces
// (mapping to a 64-bit integer), so can be used directly in database calls.
package guid

import (
	"encoding/gob"
	"fmt"
	"hash"
	"hash/fnv"
	"math"
	"math/rand/v2"
	"time"
)

var EpochString = "2022 Jan 1"
var epoch time.Time

func init() {
	var err error
	epoch, err = time.Parse("2006 Jan 2", EpochString)
	if err != nil {
		panic("Parsing epoch " + EpochString + " " + err.Error())
	}
}

// Epoch returns the beginning of the current epoch used to calculate the
// Unix-like timestamp.
func Epoch() time.Time {
	return epoch
}

// GuidGenerator is used to store information necessary to genreate a Guid. This
// should always be created using GuidGenerator; then all content added using
// the Content() methods; and then finally the Guid() method called to get the
// resulting compact instance of the guid.
type GuidGenerator struct {
	timestamp int64
	hash      hash.Hash64
	err       error
	evaluated bool
	value     int64
	encoder   *gob.Encoder
}

func NewGuidGenerator() *GuidGenerator {
	var g GuidGenerator

	duration := time.Since(epoch).Milliseconds()

	if duration/1000 > math.MaxInt32 {
		panic("Timestamp expired!")
	}

	g.timestamp = duration / 1000

	g.hash = fnv.New64()

	g.encoder = gob.NewEncoder(g.hash)

	return g.Content(duration).Content(rand.Uint64())
}

func (g *GuidGenerator) setError(err error) *GuidGenerator {
	if g.err == nil {
		g.err = err
	}
	return g
}

// Content hashes in the content to the hash portion of the Guid.  Internally
// "encoding/gob" is used to translate objects into bytestreams, so any content
// must be accessible to that package.  Additionally, a Guid type may be passed
// in.
//
// If an error has already occured in the generation pipeline, this will be
// ignored.  If an error occurs during the encodring, the error will be set,
// Errors can be read by calling the Guid() method.
//
// If the GuidGenerator has already been evaluated (by calling the Guid()
// method), this will cause the error to be set.
func (g *GuidGenerator) Content(e any) *GuidGenerator {
	if g.err == nil {
		if g.evaluated {
			g.err = fmt.Errorf("Content added after ")
		} else {
			switch v := e.(type) {
			case Guid:
				g.err = g.encoder.Encode(v.guid)
			default:
				g.err = g.encoder.Encode(e)
			}
		}
	}
	return g
}

// Guid evaluates the content of GuidGenerator into a Guid type.  The result is
// cached, so that repeated calls to Guid will not do a complete recalculation.
//
// If any errors have happened during the generation pipeline, NullGuid will be
// returned, along with the error.
//
// Once a GuidGenerator is evaluated with this method, no further content can be
// added; doing so will cause the error to be set.
func (g *GuidGenerator) Guid() (Guid, error) {
	if g.err != nil {
		return NullGuid, g.err
	}
	if !g.evaluated {
		g.value = (g.timestamp & ((1 << 31) - 1)) << 32
		g.value |= int64(g.hash.Sum64() & ((1 << 32) - 1))
		g.evaluated = true
	}
	return Guid{guid: g.value}, nil
}
