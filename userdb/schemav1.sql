CREATE TABLE users(
    userguid       integer primary key,
    username       text not null unique,
    hashedpassword text not null,
    userdata text    
) strict;
