package userdb

import (
	"errors"
	"regexp"
	"unicode"
)

func AllWhitespace(s string) bool {
	for _, ch := range s {
		if !unicode.IsSpace(ch) {
			return false
		}
	}
	return true
}

var (
	ErrHashedPasswordNotEmpty = errors.New("Hashed password must be empty")
	ErrNoUsername             = errors.New("You must supply a username")
	ErrNoPassword             = errors.New("You must supply a password")
	ErrPasswordTooShort       = errors.New("Your password is too short")
	ErrUsernameIsEmail        = errors.New("Username cannot be an email address")
	ErrUsernameIsNotEmail     = errors.New("Username must be an email address")
	ErrPasswordIncorrect      = errors.New("Password did not match")
	ErrUsernameExists         = errors.New("That username is taken")
	ErrUserNotFound           = errors.New("User not found")
	ErrEmailExists            = errors.New("That email address has an account")
	ErrInternal               = errors.New("Internal Error")
)

var emailRE = regexp.MustCompile(`^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$`)

func IsEmailAddress(s string) bool {
	return emailRE.MatchString(s)
}
