package userdb_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/icrowley/fake"
	"golang.org/x/exp/maps"

	"gitlab.com/martyros/sqlutil/types/jsondb"
	"gitlab.com/martyros/sqlutil/userdb"
)

const (
	TestPassword  = "UserDbUser00"
	TestPassword2 = "UserDbUser01"
)

var populateFakeData bool

func testNewUser(t *testing.T, db *userdb.UserDb) (userdb.User, bool) {
	user := userdb.User{
		Username: fake.EmailAddress(),
	}

	if populateFakeData {
		user.UserData.Set("RealName", fake.FullName())
		user.UserData.Set("Company", fake.Company())
		user.UserData.Set("DisplayName", fake.UserName())
		user.UserData.Set("Description", fake.Paragraphs())
	}

	populateFakeData = !populateFakeData

	for _, err := db.NewUser(&user, TestPassword); err != nil; _, err = db.NewUser(&user, TestPassword) {
		// Just keep trying random usernames until we get a new one
		if err == userdb.ErrUsernameExists {
			user.Username = fake.UserName()
			t.Logf(" User exists!  Trying username %s instead", user.Username)
			continue
		}
		t.Errorf("Creating a test user: %v", err)
		return user, true
	}

	return user, false
}

type mirrorData struct {
	users   []userdb.User
	userMap map[userdb.UserGuid]int
}

func testNewUsers(t *testing.T, db *userdb.UserDb, m *mirrorData, testUserCount int) bool {
	m.users = make([]userdb.User, testUserCount)
	m.userMap = make(map[userdb.UserGuid]int)

	for i := range m.users {
		subexit := false
		m.users[i], subexit = testNewUser(t, db)
		if subexit {
			return false
		}
		m.userMap[m.users[i].UserGuid] = i
	}

	return false
}

func testUserConstraints(t *testing.T, db *userdb.UserDb, cfg userdb.Config) {
	// Username
	t.Log("Creating with an empty username...")
	if _, err := db.NewUser(&userdb.User{}, TestPassword); err != userdb.ErrNoUsername {
		t.Errorf("ERROR: Creating with empty username got unexpected error %v!", err)
	}
	t.Log("Creating with an all-space username...")
	if _, err := db.NewUser(&userdb.User{Username: " \t "}, TestPassword); err != userdb.ErrNoUsername {
		t.Errorf("ERROR: Creating with all-space username got unexpected error %v!", err)
	}
	if cfg.UserIDIsEmail {
		t.Log("Creating with a non-email username...")
		if _, err := db.NewUser(&userdb.User{Username: fake.UserName()}, TestPassword); err != userdb.ErrUsernameIsNotEmail {
			t.Errorf("ERROR: UserIdIsEmail, but creating with non-email username got unexpected error %v!", err)
		}
	} else {
		t.Log("Creating with an email username...")
		if _, err := db.NewUser(&userdb.User{Username: fake.EmailAddress()}, TestPassword); err != userdb.ErrUsernameIsNotEmail {
			t.Errorf("ERROR: !UserIdIsEmail, but creating with email username got unexpected error %v!", err)
		}
	}

	var username string
	if cfg.UserIDIsEmail {
		username = fake.EmailAddress()
	} else {
		username = fake.UserName()
	}

	// Password
	t.Log("Creating with empty password")
	if _, err := db.NewUser(&userdb.User{Username: username}, ""); err != userdb.ErrNoPassword {
		t.Errorf("ERROR: Short password got unexpected error %v!", err)
	}

	min := cfg.MinPasswordLength
	if min == 0 {
		min = userdb.DefaultPasswordLength
	}
	if min > 1 {
		pass := TestPassword[:min-1]
		t.Logf("Creating with short password `%s`", pass)
		if _, err := db.NewUser(&userdb.User{Username: username}, pass); err != userdb.ErrPasswordTooShort {
			t.Errorf("ERROR: Short password got unexpected error %v!", err)
		}
	}
}

func TestCreate(t *testing.T) {
	dbFile, err := os.CreateTemp("", "userdbtest*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	cfg := userdb.Config{
		DataSourceName: `file:` + dbFile.Name() + `?_fk=true&mode=rwc`,
		SchemaName:     `gitlab.com/martyros/sqlutil/userdb_test`,
		UserIDIsEmail:  true,
	}

	t.Logf("Creating DB in file %s", dbFile.Name())
	db, err := userdb.Open(cfg)
	if err != nil {
		t.Errorf("ERROR: Opening database: %v", err)
		return
	}

	testUserConstraints(t, db, cfg)

	m := mirrorData{}

	if testNewUsers(t, db, &m, 10) {
		return
	}

	db.Close()

	t.Log("Opening with bogus schema name")
	{
		c2 := cfg
		c2.SchemaName = `github.com/gwd/sqlutil/userdb_test`
		if _, err := userdb.Open(c2); err == nil {
			t.Error("ERROR: Opening database with invalid schema name succeeded!")
		}
	}

	db, err = userdb.Open(cfg)
	if err != nil {
		t.Errorf("ERROR: Opening database a second time: %v", err)
		return
	}

	t.Log("Testing UserFindAll")
	if users, err := db.UserFindAll(); err != nil {
		t.Errorf("ERROR: UserFindAll: %v", err)
	} else if len(users) != len(m.users) {
		t.Errorf("ERROR: UserFindAll: Found %d users, expecting %d", len(users), len(m.users))
	} else {
		userMap := maps.Clone(m.userMap)
		for _, user := range users {
			if _, prs := userMap[user.UserGuid]; !prs {
				t.Errorf("ERROR: UserFindAll: Missing expected UserGuid %v", user.UserGuid)
			} else {
				delete(userMap, user.UserGuid)
			}
		}
		if len(userMap) != 0 {
			for _, user := range userMap {
				t.Errorf("ERROR: UserFindAll: Unexpected user %v", user)
			}
		}
	}

	t.Log("Testing SetPassword with invalid user")
	if err := db.SetPassword(&userdb.User{Username: "NotPresent"}, TestPassword); err != userdb.ErrUserNotFound {
		t.Errorf("ERROR: Expected ErrUserNotFound, got %v!", err)
	}

	t.Log("Testing ChangePassword with invalid user")
	if err := db.ChangePassword(&userdb.User{Username: "NotPresent"}, TestPassword, TestPassword2); err != userdb.ErrUserNotFound {
		t.Errorf("ERROR: Expected ErrUserNotFound, got %v!", err)
	}

	for i := range m.users {
		mu := &m.users[i]
		t.Logf("Looking up userguid %v", mu.UserGuid)
		{
			u, err := db.UserFind(mu.UserGuid)
			if err != nil {
				t.Errorf("ERROR: Finding user with Guid %v: %v", mu.UserGuid, err)
				continue
			}
			if !u.CheckPassword(TestPassword) {
				t.Errorf("ERROR: CheckPassword failed for user %v", u.Username)
			}
			if u.CheckPassword("wrong password") {
				t.Errorf("ERROR: CheckPassword succeeded with bogus password")
			}
			if !cmp.Equal(*mu, u) {
				t.Errorf("ERROR: Difference: %v", cmp.Diff(*mu, u))
			}
		}

		t.Logf("Looking up username %v", mu.Username)
		{
			u, err := db.UserFindByUsername(mu.Username)
			if err != nil {
				t.Errorf("ERROR: Finding user with username %v: %v", mu.Username, err)
				continue
			}
			if !cmp.Equal(*mu, u) {
				t.Errorf("ERROR: Difference: %v", cmp.Diff(*mu, u))
			}
		}

		t.Logf("Setting password by Guid")
		if err := db.SetPassword(mu, TestPassword2); err != nil {
			t.Errorf("ERROR: Setting password: %v", err)
		}
		{
			u, err := db.UserFind(mu.UserGuid)
			if err != nil {
				t.Errorf("ERROR: Finding user with Guid %v: %v", mu.UserGuid, err)
				continue
			}
			if !u.CheckPassword(TestPassword2) {
				t.Errorf("ERROR: CheckPassword failed after password change for user %v", u.Username)
			}
			if u.CheckPassword(TestPassword) {
				t.Errorf("ERROR: CheckPassword succeeded with old password")
			}
		}

		t.Logf("Setting password by Userid")
		{
			if err := db.SetPassword(&userdb.User{Username: mu.Username}, TestPassword); err != nil {
				t.Errorf("ERROR: Setting password: %v", err)
			}
			u, err := db.UserFind(mu.UserGuid)
			if err != nil {
				t.Errorf("ERROR: Finding user with Guid %v: %v", mu.UserGuid, err)
				continue
			}
			if !u.CheckPassword(TestPassword) {
				t.Errorf("ERROR: CheckPassword failed after password change for user %v", u.Username)
			}
			if u.CheckPassword(TestPassword2) {
				t.Errorf("ERROR: CheckPassword succeeded with old password")
			}
		}

		t.Logf("Changing password by Userid")
		{
			// Try with the wrong password first; should fail
			if err := db.ChangePassword(&userdb.User{Username: mu.Username}, TestPassword2, TestPassword); err == nil {
				t.Error("ERROR: Changing password with invalid password worked")
			} else if err != userdb.ErrPasswordIncorrect {
				t.Errorf("ERROR: Unexpected error changing with wrond old password: %v", err)
			}
			// Now try with the right password
			if err := db.ChangePassword(&userdb.User{Username: mu.Username}, TestPassword, TestPassword2); err != nil {
				t.Errorf("ERROR: Setting password: %v", err)
			}
			u, err := db.UserFind(mu.UserGuid)
			if err != nil {
				t.Errorf("ERROR: Finding user with Guid %v: %v", mu.UserGuid, err)
				continue
			}
			if !u.CheckPassword(TestPassword2) {
				t.Errorf("ERROR: CheckPassword failed after password change for user %v", u.Username)
			}
			if u.CheckPassword(TestPassword) {
				t.Errorf("ERROR: CheckPassword succeeded with old password")
			}
		}

		t.Logf("Changing password by UserGuid")
		{
			// Try with the wrong password first; should fail
			if err := db.ChangePassword(&userdb.User{UserGuid: mu.UserGuid}, TestPassword, TestPassword2); err == nil {
				t.Error("ERROR: Changing password with invalid password worked")
			} else if err != userdb.ErrPasswordIncorrect {
				t.Errorf("ERROR: Unexpected error changing with wrond old password: %v", err)
			}
			// Now try with the right password
			if err := db.ChangePassword(&userdb.User{Username: mu.Username}, TestPassword2, TestPassword); err != nil {
				t.Errorf("ERROR: Setting password: %v", err)
			}
			u, err := db.UserFind(mu.UserGuid)
			if err != nil {
				t.Errorf("ERROR: Finding user with Guid %v: %v", mu.UserGuid, err)
				continue
			}
			if !u.CheckPassword(TestPassword) {
				t.Errorf("ERROR: CheckPassword failed after password change for user %v", u.Username)
			}
			if u.CheckPassword(TestPassword2) {
				t.Errorf("ERROR: CheckPassword succeeded with old password")
			}
		}

		t.Logf("Making sure non-existend guid fails")
		if err := db.SetPassword(&userdb.User{}, "shoudlfail"); err == nil {
			t.Errorf("Setting for non-existend userid succeeded!")
		}

		t.Logf("Testing UserData")
		{
			teststrings := []string{"alpha", "beta", "gamma", "delta"}
			for i, teststring := range teststrings {
				// First intentionally do an abort after modifying the keymap to
				// make sure we get the error
				if err := db.UserModDataTx(mu, func(km *jsondb.KeyMap) error {
					var list []string
					if err, _ := km.Get("testKey", &list); err != nil {
						return err
					}

					if len(list) != i {
						return fmt.Errorf("Unexpected length: wanted %d, got %d", i, len(list))
					}

					list = append(list, teststring)
					if err := km.Set("testKey", list); err != nil {
						return err
					}

					return fmt.Errorf("Testing error")
				}); err == nil {
					t.Errorf("ERROR: Transaction should have aborted, but succeeded!")
					break
				}

				var tu userdb.User

				if i&1 == 0 {
					t.Logf("i %d, supplying UserGuid %v", i, mu.UserGuid)
					tu.UserGuid = mu.UserGuid
				} else {
					t.Logf("i %d, supplying Username %v", i, mu.Username)
					tu.Username = mu.Username
				}

				// Then do the change for real, checking to make sure that the
				// key in question was never committed
				if err := db.UserModDataTx(&tu, func(km *jsondb.KeyMap) error {
					var list []string
					if err, _ := km.Get("testKey", &list); err != nil {
						return err
					}

					if len(list) != i {
						return fmt.Errorf("Unexpected length: wanted %d, got %d", i, len(list))
					}

					list = append(list, teststring)
					if err := km.Set("testKey", list); err != nil {
						return err
					}

					return nil
				}); err != nil {
					t.Errorf("ERROR Adding teststring %s: %v", teststring, err)
					break
				}

				// Then read back the key to make sure it's what we expect
				if u, err := db.UserFind(mu.UserGuid); err != nil {
					t.Errorf("ERROR Getting user after setting userdata: %v", err)
				} else {
					var list []string
					if err, _ := u.UserData.Get("testKey", &list); err != nil {
						t.Errorf("ERROR Getting testKey: %v", err)
					} else if !cmp.Equal(list, teststrings[0:i+1]) {
						t.Errorf("ERROR Got %v, expected %v!", list, teststrings[0:i+1])
					}
				}
			}
		}

		t.Log("Deleting user")
		if err := db.DeleteUser(mu.UserGuid); err != nil {
			t.Errorf("ERROR: Deleting user %v: %v", mu.UserGuid, err)
		} else {
			if u, err := db.UserFind(mu.UserGuid); err != userdb.ErrUserNotFound {
				t.Errorf("ERROR: After deleting guid %v, UserFind found %v!", mu.UserGuid, u)
			}
			if u, err := db.UserFindByUsername(mu.Username); err != userdb.ErrUserNotFound {
				t.Errorf("ERROR: After deleting guid %v, UserFindByUsername found %v!", mu.UserGuid, u)
			}
		}

		*mu = userdb.User{}
	}
}
