package userdb

import (
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/martyros/sqlutil/types/jsondb"
)

// UserModDataTx is a helper function to help mutate a user's stored data.  The
// function will Start a transaction, read the user's data field (using
// User.Guid if available, falling back to User.Username if not), then call the
// function; if the function returns non-nil, it will then attempt to write the
// mutated keymap back to the database and commit the transaction.
//
// IMPORTANT: If the transaction fails due to a conflict, the transaction will
// attempt to repeat; which means *the function may be called several times*.
// Make sure that the function has no side effects (or that they are
// idempotent).
func (db *UserDb) UserModDataTx(u *User, f func(*jsondb.KeyMap) error) error {
	query := dialect.From(tUsers).Select(cUserdata)
	query = userQueryWhere(u, query)

	if err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		var km jsondb.KeyMap

		if err := queryGet(eq, &km, query); err != nil {
			return err
		}

		if err := f(&km); err != nil {
			return err
		}

		// NB we have to build the query inside the loop, because we may have
		// multiple iterations and need to make sure we have the most recent
		// version.
		update := dialect.Update(tUsers).Set(goqu.Record{"userdata": km})
		update = userQueryWhere(u, update)

		if res, err := queryExec[*goqu.UpdateDataset](eq, update); err != nil {
			return err
		} else if affected, err := res.RowsAffected(); err != nil {
			return err
		} else if affected != 1 {
			return fmt.Errorf("INTERNAL ERROR: Expected RowsAffected to be 1, got %d", affected)
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}
