// Package userdb implements a basic user password service in an SQL database.
package userdb

import (
	"database/sql"
	_ "embed"
	"errors"
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/guid"
	"gitlab.com/martyros/sqlutil/lifecycle"
	"gitlab.com/martyros/sqlutil/liteutil"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/martyros/sqlutil/types/jsondb"

	"golang.org/x/crypto/bcrypt"
)

const (
	DefaultSchemaName     = "gitlab.com/martyros/sqlutil/userdb#db"
	DefaultHashCost       = 10
	DefaultPasswordLength = 6
)

type UserDb struct {
	*sqlx.DB
	cfg Config
}

type Config struct {
	DataSourceName    string // Passed to sqlx.Open().
	SchemaName        string // Schema name.  Default gitlab.com/martyros/sqlutil/userdb#db.
	UserIDIsEmail     bool   // Should the UserID look like an email?  Default false.
	HashCost          int    // Hash cost to pass to bcrypt.  Default 10
	MinPasswordLength int    // Minimum password length.  Default 6.
}

//go:embed schemav1.sql
var schemav1 string

var (
	dialect = goqu.Dialect("sqlite3")

	tUsers = goqu.T("users")

	cUserGuid       = goqu.C("userguid")
	cUsername       = goqu.C("username")
	cHashedPassword = goqu.C("hashedpassword")
	cUserdata       = goqu.C("userdata")
)

// Open initializes a new UserDb instance with the provided configuration. It
// connects to the SQLite3 database specified in the DataSourceName field of the
// Config. If the SchemaName field is empty, the DefaultSchemaName is used. If
// the HashCost field is 0, the DefaultHashCost is used. If the
// MinPasswordLength field is 0, the DefaultPasswordLength is used. The function
// checks and applies the necessary database schema migrations. In case of any
// error during the process, it returns the error and ensures the database
// connection is closed. On successful initialization, it returns a pointer to
// the UserDb instance and a nil error.
func Open(cfg Config) (*UserDb, error) {
	db, err := sqlx.Open("sqlite3", cfg.DataSourceName)
	if err != nil {
		return nil, fmt.Errorf("Opening database: %w", err)
	}

	if cfg.SchemaName == "" {
		cfg.SchemaName = DefaultSchemaName
	}

	if cfg.HashCost == 0 {
		cfg.HashCost = DefaultHashCost
	}

	if cfg.MinPasswordLength == 0 {
		cfg.MinPasswordLength = DefaultPasswordLength
	}

	err = lifecycle.Schema(1, lifecycle.Recipe{Sql: schemav1}).
		SetSchemaName(cfg.SchemaName).
		Open(db)
	if err != nil {
		db.Close()
		return nil, fmt.Errorf("Init / migrate failed: %w", err)
	}

	return &UserDb{DB: db, cfg: cfg}, nil
}

func (db *UserDb) Close() {
	db.DB.Close()
	*db = UserDb{}
}

type UserGuid struct {
	guid.Guid
}

var NullUserGuid = UserGuid{guid.NullGuid}

type User struct {
	UserGuid       UserGuid
	HashedPassword string
	Username       string
	UserData       jsondb.KeyMap
}

func (cfg *Config) passwordHash(password string) (string, error) {
	if password == "" {
		return "", ErrNoPassword
	} else if len(password) < cfg.MinPasswordLength {
		return "", ErrPasswordTooShort
	}

	if hashedPasswordBytes, err := bcrypt.GenerateFromPassword([]byte(password), cfg.HashCost); err != nil {
		return "", ErrInternal
	} else {
		return string(hashedPasswordBytes), err
	}
}

// NewUser creates a new user in the UserDb and returns the newly created user's
// UserGuid. It performs several validation checks on the provided user
// information:
//   - Ensures the username is not empty or all whitespace.
//   - Ensures the username is an email address if the UserIDIsEmail option is
//     true, and not an email if false.
//   - Ensures the provided plain-text password is not empty and meets the minimum
//     length requirement.
//   - Generates a unique GUID for the user based on their username.
//   - Inserts the new user into the users table in the database.
//
// If any of these checks fail, an appropriate error is returned.
//
// Returns:
// - UserGuid: The GUID of the newly created user.
// - error: An error if any of the validation checks fail or an issue occurs
// during the database transaction.
func (db *UserDb) NewUser(user *User, password string) (UserGuid, error) {
	if user.Username == "" || AllWhitespace(user.Username) {
		return NullUserGuid, ErrNoUsername
	}

	if !db.cfg.UserIDIsEmail && IsEmailAddress(user.Username) {
		return NullUserGuid, ErrUsernameIsEmail
	} else if db.cfg.UserIDIsEmail && !IsEmailAddress(user.Username) {
		return NullUserGuid, ErrUsernameIsNotEmail
	}

	if user.HashedPassword != "" {
		return NullUserGuid, ErrHashedPasswordNotEmpty
	} else if hashedPassword, err := db.cfg.passwordHash(password); err != nil {
		return NullUserGuid, err
	} else {
		user.HashedPassword = hashedPassword
	}

	if guid, err := guid.NewGuidGenerator().Content(user.Username).Guid(); err != nil {
		return NullUserGuid, fmt.Errorf("Generating collection guid: %w", err)
	} else {
		user.UserGuid = UserGuid{guid}
	}

	if err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		_, err := eq.Exec(`
        insert into users (
            userguid,
            hashedpassword,
            username,
			userdata) values(?, ?, ?, ?)`,
			user.UserGuid,
			user.HashedPassword,
			user.Username,
			user.UserData)
		return err
	}); liteutil.IsErrorConstraintUnique(err) {
		return NullUserGuid, ErrUsernameExists
	} else if err != nil {
		return NullUserGuid, err

	}

	return user.UserGuid, nil
}

func (u *User) CheckPassword(password string) bool {
	// Don't bother checking the password if it's empty
	if password == "" {
		return false
	}

	return bcrypt.CompareHashAndPassword(
		[]byte(u.HashedPassword),
		[]byte(password)) == nil
}

type goquWhere[T any] interface {
	Where(expressions ...exp.Expression) T
}

type goquToSQLer interface {
	ToSQL() (sql string, params []interface{}, err error)
}

type goquPreparer[T goquToSQLer] interface {
	Prepared(prepared bool) T
}

// Somehow I can't believe this works

func userQueryWhere[T goquWhere[T]](u *User, q T) T {
	if u.UserGuid != NullUserGuid {
		return q.Where(cUserGuid.Eq(u.UserGuid))
	} else {
		return q.Where(cUsername.Eq(u.Username))
	}
}

// SetPassword sets the user's password.  If user.UserGuid is set, the update
// will be done according to UserGuid; otherwise Username will be used.
func (db *UserDb) SetPassword(user *User, newPassword string) error {
	hashedPassword, err := db.cfg.passwordHash(newPassword)
	if err != nil {
		return err
	}

	query := dialect.Update(tUsers).Set(goqu.Record{"hashedpassword": hashedPassword})

	query = userQueryWhere(user, query)

	sql, args, err := query.Prepared(true).ToSQL()
	if err != nil {
		return err
	}

	return txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		res, err := eq.Exec(sql, args...)
		if err != nil {
			return fmt.Errorf("Updating record for user: %w", err)
		}
		rcount, err := res.RowsAffected()
		if txutil.ShouldRetry(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("Getting rows affected: %w", err)
		}
		switch {
		case rcount == 0:
			return ErrUserNotFound
		case rcount > 1:
			return fmt.Errorf("Expected to change 1 row, changed %d", rcount)
		}

		user.HashedPassword = hashedPassword

		return nil
	})
}

// ChangePassword will check that the old password matches; and if so, change it
// to the new password.  If user.UserGuid is set, the update will be done
// according to UserGuid; otherwise Username will be used.
func (db *UserDb) ChangePassword(user *User, oldPassword string, newPassword string) error {
	newPasswordHash, err := db.cfg.passwordHash(newPassword)
	if err != nil {
		return err
	}

	checkQuery := dialect.From(tUsers).Select(cHashedPassword)

	checkQuery = userQueryWhere(user, checkQuery)

	setQuery := dialect.Update(tUsers).Set(goqu.Record{"hashedpassword": newPasswordHash})

	setQuery = userQueryWhere(user, setQuery)

	setSql, args, err := setQuery.Prepared(true).ToSQL()
	if err != nil {
		return err
	}

	return txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		var hashedPassword string
		if err := queryGet(eq, &hashedPassword, checkQuery); txutil.ShouldRetry(err) {
			return err
		} else if errors.Is(err, sql.ErrNoRows) {
			return ErrUserNotFound
		} else if err != nil {
			return fmt.Errorf("Database error: %v", err)
		} else if bcrypt.CompareHashAndPassword(
			[]byte(hashedPassword),
			[]byte(oldPassword)) != nil {
			return ErrPasswordIncorrect
		}

		res, err := eq.Exec(setSql, args...)
		if err != nil {
			return fmt.Errorf("Updating record for user: %w", err)
		}
		rcount, err := res.RowsAffected()
		if txutil.ShouldRetry(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("Getting rows affected: %w", err)
		}
		switch {
		case rcount == 0:
			return ErrUserNotFound
		case rcount > 1:
			return fmt.Errorf("Expected to change 1 row, changed %d", rcount)
		}

		user.HashedPassword = newPasswordHash

		return nil
	})
}

func (db *UserDb) DeleteUser(userguid UserGuid) error {
	return txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		res, err := eq.Exec(`
        delete from users where userguid=?`,
			userguid)
		if err != nil {
			return fmt.Errorf("Deleting record for user: %w", err)
		}
		rcount, err := res.RowsAffected()
		if txutil.ShouldRetry(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("Getting rows affected: %w", err)
		}
		switch {
		case rcount == 0:
			return ErrUserNotFound
		case rcount > 1:
			return fmt.Errorf("Expected to change 1 row, changed %d", rcount)
		}

		return nil
	})
}

func querySelect(q sqlx.Queryer, t any, query *goqu.SelectDataset) error {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return err
	} else if err = sqlx.Select(q, t, sql, args...); err != nil {
		return err
	}
	return nil
}

func queryGet(q sqlx.Queryer, t any, query *goqu.SelectDataset) error {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return err
	} else if err = sqlx.Get(q, t, sql, args...); err != nil {
		return err
	}
	return nil
}

func queryExec[T goquToSQLer](q sqlx.Execer, query goquPreparer[T]) (sql.Result, error) {
	if sql, args, err := query.Prepared(true).ToSQL(); err != nil {
		return nil, err
	} else {
		return q.Exec(sql, args...)
	}
}

func (db *UserDb) userGetWhere(query *goqu.SelectDataset) (User, error) {
	user := User{}
	err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		err := queryGet(eq, &user, query)
		if err == sql.ErrNoRows {
			err = ErrUserNotFound
		}
		return err
	})
	return user, err

}

var queryUserBase = dialect.From(tUsers).Select(cUserGuid, cUsername, cHashedPassword, cUserdata)

// UserFind retrieves a user from the UserDb by their UserGuid.
// If the user is found, it returns the User object and a nil error.
// If the user is not found, it returns a nil User object and a nil error.
// If there is any other error during the operation, it returns a nil User
func (db *UserDb) UserFind(userguid UserGuid) (User, error) {
	return db.userGetWhere(queryUserBase.Where(cUserGuid.Eq(userguid)))
}

// UserFindByUsername retrieves a user from the UserDb by their Username. If the
// user is found, it returns the User object and a nil error. If the user is not
// found, it returns a nil User object and a nil error. If there is any other
// error during the operation, it returns a nil User
func (db *UserDb) UserFindByUsername(username string) (User, error) {
	return db.userGetWhere(queryUserBase.Where(cUsername.Eq(username)))
}

func (db *UserDb) UserFindAll() ([]User, error) {
	users := []User(nil)
	if err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		return querySelect(eq, &users, queryUserBase)
	}); err != nil {
		return nil, err
	}
	return users, nil
}

// Iterate over all users, calling f(u) for each user.
// func (db *UserDb) UserIterate(f func(u *User) error) error {
// 	err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
// 		rows, err := eq.Queryx(`select * from event_users order by userid`)
// 		if err != nil {
// 			return err
// 		}
// 		defer rows.Close()

// 		processed := 0
// 		for rows.Next() {
// 			var user User
// 			if err := rows.StructScan(&user); err != nil {
// 				return err
// 			}
// 			err = f(&user)
// 			if err != nil {
// 				return err
// 			}
// 			processed++
// 		}

// 		// For some reason we often get the transaction conflict error
// 		// from rows.Err() rather than from the original Queryx.
// 		// Retrying is fine as long as we haven't actually processed
// 		// any rows yet.  If we have, throw an error.  (There's an
// 		// argument to makign this a panic() instead.)
// 		err = rows.Err()
// 		if txutil.ShouldRetry(err) {
// 			if processed == 0 {
// 				rows.Close()
// 			} else {
// 				err = fmt.Errorf("INTERNAL ERROR: Got transaction retry error after processing %d callbacks",
// 					processed)
// 			}
// 		}

// 		return err
// 	})

// 	for {

// 		return err
// 	}
// }
